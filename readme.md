# githooks
Stuff that I want to run in different stages of the commit

## pre-commit
Just a safety net to check for sensitive data before making a commit

It runs a `git diff --unified=0 --staged` and looks each line 
for sensitive words (eg. 'email', 'api-key') and, where appropriate, for a matching pattern
asking the user for confirmation.

#### Disclaimer
The list is far from complete and it's a work in progress. 
Care must be taken as to what you commit.
In case you've added sensitive information to your git repository in the past
consider following this guide: https://help.github.com/articles/remove-sensitive-data/